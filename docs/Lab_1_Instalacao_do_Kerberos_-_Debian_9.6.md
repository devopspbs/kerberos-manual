# Lab 1: Instalação do Servidor Kerberos - Debian 9.6
## Servidor - Preparação do Ambiente
- Dados Básicos Sobre o Servidor
- Instalação e Configuração de Pacotes Utilitarios
- Configurações do Hostname e Hosts

### Dados Básicos Sobre o Servidor
Para este lab, vamos criar um domínio MIT Kerberos com os seguintes recursos (editá-los para atender ao seu ambiente):
    
    Hostname: labk
    Domínio: lab.local
    FQDN:labk.lab.local
    Realm: LAB.LOCAL
    Principal Admin: labk/admin
    KDC Primário: labk.lab.local ou IP_DO_SEU_SERVIDOR
    IP: IP_DO_SEU_SERVIDOR
    Servidor NTP: a.ntp.br
    Servidor DNS: DNS_DA_SUA_REDE

> Este laboratorio foi testado e validado no Debian 9.6

### Instalação e Configuração de Pacotes Utilitarios

- NMAP
- NTP
  
#### NMAP

#### NTP

Além disso, o Kerberos é um protocolo sensível ao tempo. Portanto, se o horário do sistema local entre uma máquina cliente e o servidor for diferente em mais de cinco minutos (por padrão), a estação de trabalho não poderá ser autenticada. Para corrigir o problema, todos os hosts devem ter seu tempo sincronizado usando o mesmo servidor NTP (Network Time Protocol).

> Configuração do serviço NTP(A ser feita)


### Configurações do Hostname e Hosts

Antes de instalar o servidor Kerberos, é necessário um servidor DNS configurado corretamente para o seu domínio. Como o **Realm(reino)** do Kerberos por convenção corresponde ao nome do domínio, esta seção usa o domínio LAB.LOCAL configurado no mestre principal da documentação do DNS. Caso seu ambiente não tenha um servidor DNS, será necessaria realizar configurações nos arquivos hostname e hosts para conseguir resevolver o nome do Servidor Kerberos e Cliente, como a seguir:
Para isso acesse o arquivo **/etc/hostname** e altere o nome do host para:

```
    labk
```
Após isso acesse o arquivo **/etc/hosts** e altere as seguintes entradas:

``` 

    #Exemplo de uma entrada padrão
    127.0.0.1       localhost
    127.0.1.1       host.dominio.qualquer  host

    #Altere para
    127.0.0.1       localhost
    127.0.1.1       labk.lab.local  labk
    #Caso tenha os dados cliente pode adicionar uma 
    #linha a baixo com os dados dele
    IP_DO_SEU_CLIENTE(Ex:192.168.0.10)  labc.lab.local  labc

```

>Em um ambiente de produção é inviavel utilizar esse metodo de resolução de nomes usando arquivo hosts

>É extramamente importante o servidor consiguir resolver o nome do cliente e o cliente conseguir resolver o nome do servidor.

## Servidor - Instalação e Configuração
### Verificação do Ambiente
Para esse laboratorio, vamos criar um domínio MIT Kerberos tendo como base os seguintes parametros(editi-os para atender às suas necessidades):

- hostname: labk
- FQDN: labk.lab.local
- Realm/Reino: LAB.LOCAL
- KDC Primário: labk.lab.local (192.168.0.XX)
- Principal Admin:  labk/admin

### Instalação dos Pacotes
O primeiro passo na criação de um Kerberos Realm é instalar os pacotes krb5-kdc e rb5-admin-server. No terminal, entre com o seguinte comando:
   
```
    sudo apt install krb5-kdc krb5-admin-server
```

No final da instalação dos pacotes você tera que responder algumas perguntas, como a seguir:

**Krb5-kdc**
- A primeira pergunta é relativa ao nome do nosso Realm/Reino, nesse caso devemos responder **LAB.LOCAL**, nome esse definido para uso em nosso laboratorio:
> Refazer a imagem
- A segunda pergunta é relativa ao nome do host do servidor do Kerberos do Realm **LAB.LOCAL** difinido na questão anterior, no nosso caso, será labk, que é o nome do host do nosso laboratorio:
    [![Image from Gyazo](https://i.gyazo.com/7307dd574a01965bb7d9464b67a0360a.png)](https://gyazo.com/7307dd574a01965bb7d9464b67a0360a)

- A terceira pergunta é relativa ao nome do host do servidor administrativo do Realm **LAB.LOCAL** definido na primeira pergunta, no nosso caso, será labk, que é nome do host do nosso laboratorio, mais o servidor administrativo pode estar em um servidor de diferente do servidor que tem o banco de dados do Realm:
    [![Image from Gyazo](https://i.gyazo.com/e0f05ec38bf2dfa69342e670ddf44b75.png)](https://gyazo.com/e0f05ec38bf2dfa69342e670ddf44b75)

**Krb5-admin-server**

- Por ultimos temos um mensagem do pacote krb5-admin-server, informando que o Realm não foi criado, que isso será feito após o final da configuração, usando o utilitario **krb5_newrealm**:
    [![Image from Gyazo](https://i.gyazo.com/eacb5d0502cc94941b2d399471842459.png)](https://gyazo.com/eacb5d0502cc94941b2d399471842459)

> Ao final das configurações dos pacotes vai aparecer a seguinte mensagem de erro:
> [![Image from Gyazo](https://i.gyazo.com/bf3b695d32a0715ca09c91a7373097cc.png)](https://gyazo.com/bf3b695d32a0715ca09c91a7373097cc)
> Esse erro é normal, ele ocorre porque o Realm ainda foi criado, primeiro vamos configurar o reino para depois criar o Realm.

### Configuração
Os parametros setados com base nas perguntas feitas durante a instalação são salvas no arquivo /etc/krb5.conf. Se você precisar ajustar as configurações do Key Distribution Center (KDC) simplesmente edite o arquivo e reinicie o daemon krb5-kdc. 

> Se você precisar reconfigurar o Kerberos a partir do zero, talvez para alterar o nome da região, você pode fazer isso digitando:

> `sudo dpkg-reconfigure krb5-kdc`

Depois precisamos fazer alguns ajustes e verificações na configuração do /etc/krb5.conf
Mais primeiro vamos fazer um backup do arquivo /etc/krb5.conf:

    cp -Rfa  /etc/krb5.conf{,.bkp}

Em seguida, vamos verificar arquivo /etc/krb5.conf e deixar ele de acordo com a entradas abaixo, lembrando sempre de levar em consideração os parametros do seu ambiente:

    nano /etc/krb5.conf
    
    [libdefaults]
            default_realm = LAB.LOCAL
            forwardable = true
            proxiable = true
    
    [realms]
            labxx.local = {
                    #Nas linhas em destaque é importante que as entradas kdc e o admin_server 
                    #tenham o FQDN dos respesctivos servidores.
                    ###############################
                    kdc = labk.lab.local
                    admin_server = labk.lab.local
                    ###############################
            }
    
    [domain_realm]
            .lab.local = LAB.LOCAL
            lab.local = LAB.LOCAL
    
    [logging]
            kdc = FILE:/var/log/krb5/kdc.log
            admin_server = FILE:/var/log/krb5/kadmin.log
            default = FILE:/var/log/krb5/klib.log

Continuando, vamos criar o diretório para armazenar os logs e configurar o logrotate para o kerberos:

    mkdir /var/log/krb5
    
    nano /etc/logrotate.d/krb5
    
    /var/log/krb5/kadmin.log /var/log/krb5/kdc.log /var/log/krb5/klib.log {
            daily
            missingok
            rotate 7
            compress
            delaycompress
            notifempty
    }

### Criando o Realm
A seguir, vamos criar um novo Realm com o utilitário kdb5_newrealm:

    sudo krb5_newrealm

> Por padrão, o território é criado a partir do nome de domínio do KDC.

> **krb5_newrealm**   This  script  attempts  to  create  a  Kerberos  realm.  It assumes that none of the realm components  exists,  except  for  the  **/etc/krb5.conf**  file.  (Normally   this   file   is automatically  generated  at  package  installation,  but if you skipped the configuration step, you will need to manually generate this  file  before  running  krb5_newrealm.)   It creates  the  database,  initializes  the  stash file in **/etc/krb5kdc/stash** containing the master key for the database, starts the KDC and Kerberos admin server, and creates a  stub **/etc/krb5kdc/kadm5.acl** file.

> Se os procedimentos anteriores foram executados corretamente, é para termos nesse ponto, o serviço do Kerberos funcionando, pronto para adicionarmos os usuários e realizar as configurações relativas ao gerenciamento do serviço.

Para gerênciar o KDC será necessário um usuário administrador ou **Principal administrador**. Recomenda-se usar um nome de usuário diferente do seu nome de usuário diário. Usando o utilitário kadmin.local em um terminal, digite:

    sudo kadmin.local
    Autenticação como roota/admin@LAB.LOCAL principal com senha. 
    kadmin.local:$ addprinc lab/admin
    AVISO: não há política especificada para lab/admin@LAB.LOCAL; padrão para sem política
    Digite a senha para o principal "lab/admin@LAB.LOCAL":
    Re-digite a senha para principal "lab/admin@LAB.LOCAL":
    Criado principal "lab/admin@LAB.LOCAL".
    kadmin.local:$ q

### Definindo permissões
No exemplo acima, lab é o Principal , / admin é uma Instância e @LAB.LOCAL significa o reino. O Principal, também conhecido como o Principal usuário , seria lab@LAB.LOCAL, e deveria ter apenas direitos de usuário normais.

Após isto, o novo usuário administrador precisa ter as permissões apropriadas na Lista de Controle de Acesso (ACL). As permissões são configuradas no arquivo **/etc/krb5kdc/kadm5.acl**:

    lab/admin@LAB.LOCAL 		*
    */admin						*
    admin						*

Esta entrada concede a lab/admin a capacidade de executar qualquer operação sobre qualquer Principal do Realm. Você pode configurar entidades com privilégios mais restritivos, o que é conveniente se você precisar de um Principal administrador que uma equipe júnior possa usar nos clientes Kerberos. Por favor, veja a **página do manual kadm5.acl** para detalhes.

Agora reinicie o krb5-admin-server para que a nova ACL tenha efeito:

    sudo systemctl restart krb5-admin-server.service

### Adicionando Principals e Realizando Operações Básicas
O novo usuário principal pode ser testado utilizando o kinit utility:

    kinit lab/admin
    lab/admin@LAB.LOCAL's Password:

Após digitar a senha, utilize o utilitário klist para exibir informações sobre o Ticket Granting Ticket (TGT):

    klist
    Armazenamento de credenciais FILE:/tmp/krb5cc_1000
            Gerente: lab/admin@LAB.LOCAL
    
      Emitida           Expira          Gerente
    13 de Jul 17:53:34  14 de Jul 03:53:34  krbtgt/LAB.LOCAL@LAB.LOCAL

Onde o nome do arquivo de cache krb5cc_1000 é composto pelo prefixo krb5cc_ e o ID do usuário (uid), que nesse caso é 1000 . 

> É altamente recomendável que seus usuários autenticados na rede tenham seu uid em um intervalo diferente (digamos, começando em 5000) do que o de seus usuários locais.

Para destruir ou limpar os tickts em cache usamos o kdestroy:

    kdestroy

Podemos usar a interface administrativa **kadmin** para listar as todos os principals do KDC:

    kadmin -p lab/admin
    kadmin:  listprincs
    K/M@LAB.LOCAL
    lab/admin@LAB.LOCAL
    host/auth.lab.local@LAB.LOCAL
    kadmin/admin@LAB.LOCAL
    kadmin/auth.lab.local@LAB.LOCAL
    kadmin/changepw@LAB.LOCAL
    kiprop/auth.lab.local@LAB.LOCAL
    krbtgt/LAB.LOCAL@LAB.LOCAL
    kadmin: _

Na saída, temos o principal K/M@LAB.LOCAL, que é o Master Key do Kerberos, ele é criando junto com o Realm, e contem a senha mestre Realm, além dele também temos o krbtgt/LAB.LOCAL@LAB.LOCAL que é o responsável pelo tickt que autentica o usuário.
Também podemos usar o comando **getprinc** para obter informações sobre um principal especifico, como abaixo:

    kadmin: getprinc krbtgt/LAB.LOCAL@LAB.LOCAL
    Principal: krbtgt/LAB.LOCAL@LAB.LOCAL
    Expiration date: [never]
    Last password change: [never]
    Password expiration date: [never]
    Maximum ticket life: 0 days 10:00:00
    Maximum renewable life: 7 days 00:00:00
    Last modified: sex dez 07 11:09:11 -02 2018 (db_creation@LAB.LOCAL)
    Last successful authentication: [never]
    Last failed authentication: [never]
    Failed password attempts: 0
    Number of keys: 2
    Key: vno 1, aes256-cts-hmac-sha1-96
    Key: vno 1, aes128-cts-hmac-sha1-96
    MKey: vno 1
    Attributes: REQUIRES_PRE_AUTH LOCKDOWN_KEYS
    Policy: [none]
    kadmin: q

### Adicionando Host/Serviços **Host princ & keytab**
Precisaremos criar um Principal para o host do servidor e um keytab para ele poder também acessar serviços e fazer seus serviços usarem a autenticação do kerberos, assim devemos realizar os seguinte procedimentos:

    kadmin -p lab/admin
    kadmin: addprinc -randkey host/labk.lab.local
    kadmin: ktadd host/labk.lab.local
    kadmin: q


> O parâmetro -randkey indica que o principal e do tipo que não utiliza senha para se autenticar.
> O comando kadmin deve ser preferêncialmente ser executado em modo root/sudo, porque algumas operações precisão ser gravadas em arquivos com permissões exclusivas do root.

### Observações
Pode ser necessário incluir uma entrada no /etc/hosts para o KDC para que o cliente possa localizar o KDC. Por exemplo:

    192.168.0.XX   labk.lab.local       labk

Substituindo 192.168.0.XX pelo endereço IP do seu KDC. Isso geralmente acontece quando você tem um território do Kerberos abrangendo diferentes redes separadas por roteadores.

Porém, a melhor maneira de permitir que os clientes determinem automaticamente o KDC para o Domínio está usando registros SRV de DNS. Adicione o seguinte ao /etc/named/db.example.com:

    _kerberos._udp.EXAMPLE.COM.     IN SRV 1  0 88  kdc01.example.com.
    _kerberos._tcp.EXAMPLE.COM.     IN SRV 1  0 88  kdc01.example.com.
    _kerberos._udp.EXAMPLE.COM.     IN SRV 10 0 88  kdc02.example.com. 
    _kerberos._tcp.EXAMPLE.COM.     IN SRV 10 0 88  kdc02.example.com. 
    _kerberos-adm._tcp.EXAMPLE.COM. IN SRV 1  0 749 kdc01.example.com.
    _kpasswd._udp.EXAMPLE.COM.      IN SRV 1  0 464 kdc01.example.com.

Seu novo domínio Kerberos está agora pronto para autenticar clientes.