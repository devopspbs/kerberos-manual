# Bem vindos ao Kerberos Manual

Esse manual é uma iniciativa do grupo DevOpsPBS para consolidar e compartilhar conhecimento sobre a utilização do Kerberos.

O desenvolvimento desse manual começou oficialmente no meetup do dia 08/12/2018.

O objetivo desse projeto é produzir conteúdo em português, mesmo que básico, sobre o Kerberos.

>Autor:**[DevOpsPBS](https://gitlab.com/devopspbs/)**

>**[Site do Projeto](https://devopspbs.gitlab.io/kerberos-manual/)**

## Contribuidores
- [Thiago Almeida](https://gitlab.com/thiagolsa)
- [Tiago Rocha](https://gitlab.com/tiagorocha)
- [João Melho](https://gitlab.com/joaomello77)

## Licença
CC-BY-4.0