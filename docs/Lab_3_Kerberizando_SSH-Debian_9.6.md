# Lab 3: Kerberizando  SSH - Debian 9.6
## Kerberizando o SSH
#### Configurações no Servidor
Primeiramente vamos configurar o sshd no **servidor** para aceitar autenticação de host clientes via tickts do kerberos, para isso vamos modificar as seguintes entradas do arquivo **/etc/ssh/sshd_config**:

    #A opção abaixo deve estar habilitada para que a troca de senhas via ssh
    # ocorra adequadamente.
    ChallengeResponseAuthentication yes
    # Opções para autenticação via Kerberos:
    KerberosAuthentication yes
    KerberosOrLocalPasswd yes
    KerberosTicketCleanup yes
    # Opções para aunteticação via GSSAPI:
    GSSAPIAuthentication yes
    GSSAPICleanupCredentials yes

Em seguida precisamos reiniciar o serviço  sshd

     systemctl restart sshd.service

Após as configurações do serviço ssh, devemos criar, caso não exista, o usuário local **labteste** no **servidor**:

    useradd -u 2000 -d /home/labteste -s /bin/bash labteste

> Kerberos precisa ter o usuário cadastrado no kerberos e no hostlocal, até então tinhamos criado o usuário labteste apenas no cliente.

#### Configurações no Cliente
Agora no **cliente** vamos ajustar as entradas do arquivo **/etc/ssh/ssh_config**, para ficarem como abaixo:

    StrictHostKeyChecking no
    GSSAPIAuthentication yes
    GSSAPIDelegateCredentials yes

### Teste
Agora vamos precisar realizado alguns passos no **cliente**, executando os seguinte comandos:
Para verificar os tickts existente:

    klist

Caso tenha tenha tickts no cache, limpe-os com:

    kdestroy

Em seguida, autentique-se com usuário **labteste** e verifique se foi gerado o tickt:

    kinit labteste
    klist

Com tickt gerado agora vamos logar no servidor usando **ssh** com usuário **labteste** e se tivermos feito um bom trabalho, não ira precisar digitar senha para o usuário, o serviço ssh usará um tickt para validar a autenticidade do usuário:

    ssh labteste@lab.local
    ou
    ssh labteste@192.168.0.XX

> Caso queira usar o **labteste** como usuário local, devemos criar uma senha para o mesmo no host desejado, e o PAM vai analisar usuário/senha e dependendo da combinação ela vai decidir qual vai ser metodo de autenticação usar.
> Comando para definir a senha do usuário **labteste**:
    passwd labteste